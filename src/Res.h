/* This file is part of the Marble Marcher (https://github.com/HackerPoet/MarbleMarcher).
* Copyright(C) 2018 CodeParade
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include <string>

static const int num_level_music = 4;
static const char vert_glsl[] = "assets/vert.glsl";
static const char frag_glsl[] = "assets/frag.glsl";
static const char Aileron_Bold_ttf[] = "assets/Aileron-Bold.otf";
static const char menu_click_wav[] = "assets/menu_click.wav";
static const char count_go_wav[] = "assets/count_go.wav";
static const char arrow_png[] = "assets/arrow.png";
static const char goal_wav[] = "assets/goal.wav";
static const char bounce1_wav[] = "assets/bounce1.wav";
